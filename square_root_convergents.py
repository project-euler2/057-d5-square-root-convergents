from fractions import Fraction
import time
start_time = time.time()

import sys
sys.setrecursionlimit(10000)

def square_root_formula(num_recursion):
    if num_recursion == 1:
        return Fraction(0.5)
    else : 
        return Fraction(1/(2 + square_root_formula(num_recursion - 1)))

def square_root_convergent(upper_limit):
    num_sup_den = 0
    for i in range(1,upper_limit):
        if len(str((1+ square_root_formula(i)).as_integer_ratio()[0])) > len(str((1+ square_root_formula(i)).as_integer_ratio()[1])) :
            num_sup_den += 1
    return num_sup_den

print(square_root_convergent(1000))
print(f"--- {(time.time() - start_time):.10f} seconds ---" )